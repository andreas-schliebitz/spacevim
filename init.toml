#=============================================================================
# dark_powered.toml --- dark powered configuration example for SpaceVim
# Copyright (c) 2016-2017 Wang Shidong & Contributors
# Author: Wang Shidong < wsdjeg at 163.com >
# URL: https://spacevim.org
# License: GPLv3
#=============================================================================

# All SpaceVim option below [option] section
[options]
# set spacevim theme. by default colorscheme layer is not loaded,
# if you want to use more colorscheme, please load the colorscheme
# layer
colorscheme = "one"
colorscheme_bg = "dark"
# Disable guicolors in basic mode, many terminal do not support 24bit
# true colors
enable_guicolors = true
# Disable statusline separator, if you want to use other value, please
# install nerd fonts
statusline_separator = "arrow"
statusline_inactive_separator = "arrow"
buffer_index_type = 4
enable_tabline_filetype_icon = true
enable_statusline_mode = false
bootstrap_before = "myspacevim#before"
bootstrap_after = "myspacevim#after"
filemanager = "nerdtree"
automatic_update = true
project_rooter_patterns = []
project_rooter_automatically = 0

# [[custom_plugins]]
# repo = "https://github.com/vim-pandoc/vim-pandoc"
# merged = false
#
# [[custom_plugins]]
# repo = "https://github.com/vim-pandoc/vim-pandoc-syntax"
# merged = false

# Enable autocomplete layer
[[layers]]
name = "autocomplete"
auto_completion_return_key_behavior = "complete"
auto_completion_tab_key_behavior = "smart"

[[layers]]
name = "lsp"
filetypes = [
  "typescript",
  "typescriptreact",
  "javascript",
  "javascriptreact",
  "python",
  "c",
  "cpp",
  "css",
  "html",
  "sh",
  "dockerfile"
]
enabled_clients = [
  "pyright",
  "clangd",
  "cssls",
  "html",
  "bashls",
  "dockerls",
  "intelephense",
  "tsserver"
]

[[layers]]
name = "checkers"

[[layers]]
name = "shell"
default_position = 'top'
default_height = 30

[[layers]]
name = "colorscheme"

[[layers]]
name = "format"

[[layers]]
name = "git"
git_plugin = "fugitive"

[[layers]]
name = "lang#python"
format_on_save = true
enabled_linters = ["pylint"]

[[layers]]
name = "lang#c"
format_on_save = true
enable_clang_syntax_highlight = true
clang_executable = "/usr/bin/clang++"
clang_flag = ['-Wall', '-Wextra',  '-Wpedantic']
[layer.clang_std]
  c = "c11"
  cpp = "c++17"
  objc = "c11"
  objcpp = "c++17"

[[layers]]
name = "lang#java"
java_formatter_jar = "~/.SpaceVim.d/java/google-java-format-1.9-all-deps.jar"
format_on_save = true
java_file_head = [
  '/**',
  ' * TODO: Describe what the code in this file does.',
  ' *',
  ' * @author <a href="mailto:a.schliebitz@hs-osnabrueck.de">Andreas Schliebitz</a>',
  ' * @created `strftime("%Y-%m-%d %H:%M:%S")`',
  ' * @version 1.0',
  ' */',
  ''
]

[[layers]]
name = "lang#javascript"
auto_fix = true
enable_flow_syntax = true
format_on_save = true

[[layers]]
name = "lang#typescript"
typescript_server_path = "/usr/bin/typescript-language-server"

[[layers]]
name = "lang#json"
conceal = false
concealcursor = ""

[[layers]]
name = "lang#markdown"
listItemIndent = "tab"
enableWcwidth = true
enabled_formater = ["remark", "prettier"]

[[layers]]
name = "lang#latex"

[[layers]]
name = "lang#html"
emmet_leader_key = "<C-e>"
emmet_filetyps = ['html']

[[layers]]
name = "lang#xml"

[[layers]]
name = "lang#sh"

[[layers]]
name = "lang#toml"

[[layers]]
name = "lang#php"

[[layers]]
name = "lang#dockerfile"

[[layers]]
name = "lang#extra"

# [[layers]]
# name = "gtags"
# gtagslabel = "ctags"
# auto_update = true
