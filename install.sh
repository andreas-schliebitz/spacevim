#!/usr/bin/env bash

SPACEVIM_VERSION="${1}"

[[ -z "${SPACEVIM_VERSION}" ]] && SPACEVIM_VERSION="2.2.0"

GIT_REPO='https://gitlab.com/andreas-schliebitz/spacevim/-/raw/master'

sudo apt update \
  && sudo apt install --no-install-recommends -y \
      wget \
      curl \
      git \
      neovim \
      global \
      clang \
      clangd \
      php \
      npm \
      nodejs \
      python3-pip \
      python-is-python3 \
      universal-ctags \

curl -sLf https://spacevim.org/install.sh \
  | bash \
  && cd ~/.SpaceVim \
  && git checkout "tags/v${SPACEVIM_VERSION}" \
  && cd - || exit

mkdir -p ~/.SpaceVim.d/autoload

wget -O ~/.SpaceVim.d/init.toml "${GIT_REPO}/init.toml"
wget -O ~/.SpaceVim.d/autoload/myspacevim.vim "${GIT_REPO}/autoload/myspacevim.vim"

sudo ln -s "${HOME}/.SpaceVim.d" /root/.SpaceVim.d
sudo ln -s "${HOME}/.SpaceVim" /root/.SpaceVim
sudo ln -s "${HOME}/.vim" /root/.vim
sudo ln -s "${HOME}/.nvim" /root/.nvim
sudo ln -s "${HOME}/.config/nvim" /root/.config/nvim

sudo npm install -g \
  dockerfile-language-server-nodejs \
  vscode-html-languageserver-bin \
  vscode-css-languageserver-bin \
  eslint \
  prettier \
  remark \
  remark-cli \
  remark-stringify \
  remark-frontmatter \
  wcwidth \
  intelephense \
  pyright \
  bash-language-server \
  javascript-typescript-langserver \
  typescript-language-server \
  neovim

sudo apt install --no-install-recommends -y \
  pylint \
  black \
  autoflake \
  python3-coverage \
  python3-msgpack \
  python3-pynvim
