#!/usr/bin/env bash

rm -rf "${HOME}/.SpaceVim"
rm -rf "${HOME}/.SpaceVim.d"
rm -rf "${HOME}/.vim"
rm -rf "${HOME}/.nvim"
rm -rf "${HOME}/.config/nvim"
rm -rf "${HOME}/.cache/SpaceVim"

sudo rm -rf /root/.SpaceVim.d
sudo rm -rf /root/.SpaceVim
sudo rm -rf /root/.vim
sudo rm -rf /root/.nvim
sudo rm -rf /root/.config/nvim
