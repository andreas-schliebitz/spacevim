function! myspacevim#before() abort
  nnoremap <silent> <C-Left> :vertical resize +3<CR>
  nnoremap <silent> <C-Right> :vertical resize -3<CR>
  nnoremap <silent> <C-Up> :resize +3<CR>
  nnoremap <silent> <C-Down> :resize -3<CR>

  map <C-a> <esc>ggVG<CR>
  map <Leader>th <C-w>t<C-w>H
  map <Leader>tk <C-w>t<C-w>K

  let g:tex_flavor = "lualatex"
  let g:vimtex_view_general_viewer = 'okular'
  let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
  let g:vimtex_view_general_options_latexmk = '--unique'

  let g:neoformat_python_black = {
    \ 'exe': 'black',
    \ 'stdin': 1,
    \ 'args': ['-q', '-'],
    \ }
  let g:neoformat_enabled_python = ['black']
endfunction

function! myspacevim#after() abort
  set wrap
  set splitbelow splitright
  set laststatus=2
  set cursorline

  set spelllang=de,en

  " Indentations using spaces
  " set expandtab
  " set smarttab
  " set shiftwidth=4
  " set tabstop=4

  " Indentations using tabs
  " set noexpandtab
  " set smartindent
  " set shiftwidth=4
  " set tabstop=2
endfunction
